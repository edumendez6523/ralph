#!/usr/bin/env bash

echo "==> Dropping project database"
dropdb hirelmns

echo "==> Creating project database"
createdb hirelmns

echo "==> Migrate to current state"
python manage.py migrate

echo "==> Done!"
