from .base import *

DEBUG = False

ALLOWED_HOSTS += ('*', )

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INTERNAL_IPS = ('127.0.0.1', )

API_URL = os.getenv('API_URL')
