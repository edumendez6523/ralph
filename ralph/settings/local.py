from .base import *

DEBUG = True

ALLOWED_HOSTS += ('*', )
INTERNAL_IPS = ('127.0.0.1', )

INSTALLED_APPS += ('django_extensions', 'debug_toolbar')
MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware', )

API_URL = 'http://127.0.0.1:8000/api/'
